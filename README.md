
## What does the function do?

The function collects all balances from each shopify store and insert them to a bigquery table.
Project: waterdrop-dataplatform
Dataset: finance_exports
Table: shopify_balances

### How do I get set up? ###

`pip install -r /path/to/requirements.txt`

### Start the local cloud function testing framework for the function
 - set the IS_LOCAL variable in main.py to a non None value, for local testing
 - switch in terminal to folder where main.py is located
 - Command: `functions-framework --target=update_shopify_balances`

### How should the Request look like?
* Local
 - `curl http://localhost:8080 -H "Content-Type:application/json"`
* Cloud
    Invoke function with:
    - `curl -X POST HTTP_TRIGGER_ENDPOINT -H "Content-Type:application/json"`
    - HTTP_TRIGGER_ENDPOINT: https://europe-west3-waterdrop-dataplatform.cloudfunctions.net/cf-finance-update-shopify-balances