from datetime import datetime
from flask import escape
import pytz

from pandas.io import gbq
import requests
import json
import pandas as pd
import pandas_gbq as pdg
import numpy as np
import os
import tqdm
from  google.cloud import bigquery
from google.oauth2 import service_account
from google.auth import default
import yaml

import time 
import multiprocessing

# Import the Secret Manager client library.
from google.cloud import secretmanager

def credentials():
    # To use this file locally set $IS_LOCAL=1 and populate environment variable $GOOGLE_APPLICATION_CREDENTIALS with path to keyfile
    # Get Application Default Credentials if running in Cloud Functions
    if os.getenv("IS_LOCAL") is None:
        credentials, project = default(scopes=["https://www.googleapis.com/auth/cloud-platform"])
    else:
        #Get project root path and cedentials
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__)) # This is your Project Root
        #Service account as env variable
        sa = os.path.join(ROOT_DIR, 'waterdrop-dataplatform-68bd89470d08.json')
        credentials = service_account.Credentials.from_service_account_file(sa, scopes=["https://www.googleapis.com/auth/cloud-platform"],
        )
    return credentials

def access_secret_version(project_id, version_id, store):
    store = store.lower()
    secret_id = f'{store}_shopify_python_script_app'
    """
    Access the payload for the given secret version if one exists. The version
    can be a version number as a string (e.g. "5") or an alias (e.g. "latest").
    """
    # Create the Secret Manager client.
    client = secretmanager.SecretManagerServiceClient(credentials=credentials())
    # Build the resource name of the secret version.
    name = f"projects/{project_id}/secrets/{secret_id}/versions/{version_id}"
    # Access the secret version.
    response = client.access_secret_version(request={"name": name})
    secret = response.payload.data.decode("UTF-8")
    api_key = json.loads(secret)['api_key']
    password = json.loads(secret)['password']
    return(api_key, password)

def main(store, bq_project):
    print(store)
    store = store.lower()
    secret_version_id = 1
    store_names = {
        'dach':'waterdrop-official',
        'es':'waterdrop-es',
        'fr':'https-waterdrop-official-fr',
        'it':'waterdrop-it',
        'nl':'waterdrop-nl',
        'eu':'waterdrop-europe',
        'uk':'waterdrop-official-eu',
        'us':'waterdrop-us',
    }
    store_name = store_names[store]
    #Google settings
    client = bigquery.Client(credentials=credentials())

    #Klaviyo settings
    api_key, api_password = access_secret_version(bq_project, secret_version_id, store)
    url = f'https://{api_key}:{api_password}@{store_name}.myshopify.com/admin/api/2021-07/shopify_payments/balance.json'

    response = requests.request("GET", url)
    json_resp = json.loads(response.text)

    currency =  json_resp['balance'][0]['currency']
    amount = json_resp['balance'][0]['amount']
    utc_now = pytz.utc.localize(datetime.utcnow())
    pst_now = utc_now.astimezone(pytz.timezone("Europe/Vienna"))
    print(pst_now)
    entry = [store.upper(), pst_now, currency, amount]
    return entry


def update_shopify_balances(request):
    bq_project = 'waterdrop-dataplatform'
    bq_dataset  = 'finance_exports'
    bq_table = f'shopify_balances'

    ###################################
    #os.environ["IS_LOCAL"] = 'true'
    ###################################

    stores = ['DACH', 'ES', 'EU', 'FR', 'IT', 'NL', 'UK', 'US']
    #prepare list of tuples for calling api in multiprocessing
    query_list = [(store, bq_project) for store in stores]
    entries = []

    #get current second of the actual minute and 2 second so the execution starts at 00:59 seconds
    now = time.localtime().tm_sec + 4
    time.sleep((60-now))

    with multiprocessing.Pool(multiprocessing.cpu_count()) as p:
        #balance = main(store, bq_project)
        #entries.append(balance)
        completed = p.starmap(main, query_list)
    entries.extend(completed)
    print(entries)
    df = pd.DataFrame(columns=['store', 'datetime', 'currency', 'amount'], data=entries)
    schema = [
                        {'name':'store', 'type':'STRING'},
                        {'name':'datetime', 'type':'DATETIME'},
                        {'name':'currency', 'type':'STRING'},
                        {'name':'amount', 'type':'FLOAT'},
                        ]
    df.to_gbq(f"{bq_dataset}.{bq_table}",project_id = bq_project, if_exists='append',table_schema=schema, progress_bar=True)
    utc_now = pytz.utc.localize(datetime.utcnow())
    pst_now = utc_now.astimezone(pytz.timezone("Europe/Vienna"))
    return f'Updated balances on {pst_now}'

update_shopify_balances('')